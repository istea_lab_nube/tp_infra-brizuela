#con AS damos un alias a la imagen que vamos a crear
FROM ubuntu/apache2 AS site01
#creamos un sitio de ejemplo en apache con el mensaje "Hola, soy la pagina 1"
COPY ./page01/index.html /var/www/html/

#con AS damos un alias a la imagen que vamos a crear
FROM ubuntu/apache2 AS site02
#creamos un sitio de ejemplo en apache con el mensaje "Hola, soy la pagina 2"
COPY ./page02/index.html /var/www/html/

#con AS damos un alias a la imagen que vamos a crear
FROM ubuntu/nginx AS balancer01
#copiamos los archivos de configuracion del balanceador en la configuracion del sitio por default de nginx
COPY ./lb01/default /etc/nginx/sites-available/
#exponemos el puerto 80 que nos va a servir para acceder desde nuestro host via navegador o consola
EXPOSE 80
