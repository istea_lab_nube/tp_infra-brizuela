## Balanceador de carga NGINX + Docker

-Clonamos nuestro repositorio y nos dirigimos a la raiz del proyecto

>**clonar repositorio:** git clone https://gitlab.com/istea_lab_nube/tp_infra-brizuela.git \
>**ir a directorio:** cd tp_infra-brizuela/ 

-Para el despliegue del balanceador con nginx crearemos imagenes que configuramos en el archivo "dockerfile". Dentro del archivo veremos las tres imagenes base y los nombres y configuraciones con que se crearán
 
>**nombre de imagen: imagen base**  
>apache01: ubuntu/apache2 \
>apache02: ubuntu/apache2 \
>nginx01: ubuntu/nginx 

Ejecutamos nuestro docker file

```
$ docker build -t apache01 --target site01 .
$ docker build -t apache02 --target site02 .
$ docker build -t nginx01 --target balancer01 .
```

Luego de que se hayan creado las imagenes procedemos a crear los contenedores utilizando docker-compose

```$ docker-compose up -d```

Chequeamos la creación y estado de los contenedores creados

```
$ docker ps -a
CONTAINER ID   IMAGE      COMMAND                  CREATED        STATUS        PORTS                                   NAMES
796483c6e5ed   nginx01    "/docker-entrypoint.…"   13 hours ago   Up 13 hours   0.0.0.0:8082->80/tcp, :::8082->80/tcp   balancer.app-3
b56267f870a5   apache02   "apache2-foreground"     13 hours ago   Up 13 hours   80/tcp                                  balancer.app-2
765c52eb6f80   apache01   "apache2-foreground"     13 hours ago   Up 13 hours   80/tcp                                  balancer.app-1
``` 

-Se montaran los directorios para acceder a los archivos de configuración utilizados por nginx (el balanceador) y apache (los sitios)

-Podemos ver la confiración del balanceador en el directorio lb01, allí se encuentra el grupo de servidores a balancear y el modo de balanceo

```
upstream docker-backends {
        server balancer.app-1; 
        server balancer.app-2;  
	least_conn;

``` 

>**Metodos de balanceo de carga** 
>
>**Round Robin**: las solicitudes se distribuyen de manera uniforme. Es el metodo por default 
> 
>**least_conn**: se envía la solicitud al servidor con menos cantidad de conexiones activas
>
>**ip_hash**: se determina a que servidor se envía a partir de la IP del cliente. El metodo garantiza que las solicitudes del mismo cliente vayan al mismo servidor
>
>**hash**: se envía la solicitud a partir de una clave definida por el usuario. Puede ser dirección de IP y puerto, cadenas de texto, variables, etc.
>
>**random**: cada solicitud se enviará a un servidor seleccionado aleatoriamente



-el balanceador estará expuesto por el puerto 8082 y lo redigiremos por un puerto de nuestra preferencia (utilizaremos nuestro localhost si usamos **virtualbox + red NAT + puerto reenviado** , o la **IP publica + puerto expuesto en el cloud**) 

-En docker se creará una red nueva, la cual alojará los tres contenedores utilizados para el TP

-La estructura del directorio alojado gitlab es la siguiente:

directorio del proyecto **tp_infra-brizuela**


```
.
├── README.md
├── docker-compose.yaml
├── dockerfile
├── lb01
│   └── default
├── page01
│   └── index.html
└── page02
    └── index.html

``` 
